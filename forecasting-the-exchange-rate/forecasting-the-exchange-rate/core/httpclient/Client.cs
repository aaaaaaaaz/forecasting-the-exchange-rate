﻿using forecasting_the_exchange_rate.Models;
using forecasting_the_exchange_rate.poco;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;

namespace forecasting_the_exchange_rate.core.httpclient
{
    public class Client : IClient
    {
        private readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly string RATE_URL = "https://api.exchangeratesapi.io/";
        private readonly string BASE = "?base=PLN";
        private readonly DateTime FIRST_DATE = new DateTime(2018, 1, 1);

        public List<Currency> GetCurrenciesByDate(DateTime DateTime)
        {
            Log.Info("GetCurrenciesByDate() in Client - start");
            if (DateTime.Date < FIRST_DATE)
            {
                throw new DateTooPastException("Date cannot be earlier than 2018-01-01");
            }
            List<Currency> Currencies = GetRatesByUrl(RATE_URL + DateTime.ToString("yyyy-MM-dd") + BASE);
            Log.InfoFormat("Currencies size {0} by date {1}", Currencies.Count, DateTime.Date);
            Log.Info("GetCurrenciesByDate() in Client - finish");
            return Currencies;
        }

        private List<Currency> GetRatesByUrl(string Url)
        {
            HttpWebRequest HttpWebRequest = GetHttpRequest(Url, "GET");
            HttpWebResponse WebResp = GetWebResponse(HttpWebRequest);
            if (WebResp == null)
            {
                Log.Warn("GetRatesByUrl() in Client - HttpWebResponse is null");
                return new List<Currency>();
            }
            string Response = GetResponseAsString(WebResp);
            return GetDeserializeObject(Response);
        }

        private HttpWebRequest GetHttpRequest(string Url, string HttpMethod)
        {
            HttpWebRequest HttpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format(Url));
            HttpWebRequest.Method = HttpMethod;
            return HttpWebRequest;
        }

        private HttpWebResponse GetWebResponse(HttpWebRequest HttpWebRequest)
        {
            HttpWebResponse WebResp = null;
            try
            {
                WebResp = (HttpWebResponse)HttpWebRequest.GetResponse();
            }
            catch (WebException e)
            {
                Log.Warn("GetWebResponse() - Resource is not available", e);
            }
            return WebResp;
        }

        private string GetResponseAsString(HttpWebResponse WebResp)
        {
            string Response = null;
            using (Stream Stream = WebResp.GetResponseStream())
            {
                StreamReader Reader = new StreamReader(Stream, System.Text.Encoding.UTF8);
                Response = Reader.ReadToEnd();
            }
            return Response;
        }

        private List<Currency> GetDeserializeObject(String Response)
        {
            List<Currency> Currencies = new List<Currency>();
            RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(Response);
            Rates Rates = RootObject.rates;
            PropertyInfo[] properties = Rates.GetType().GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (PropertyInfo property in properties)
            {
                Currencies.Add(new Currency(property.Name, Convert.ToDecimal(property.GetValue(Rates)), DateTime.Today.Date));
            }
            return Currencies;
        }

        public List<Currency> GetCurrenciesByStartAndEndDate(DateTime Start, DateTime End)
        {
            Log.Info("GetCurrenciesByStartAndEndDate() in Client - start");
            List<Currency> Currencies = new List<Currency>();
            while (Start.Date <= End.Date)
            {
                List<Currency> CurrenciesByDate;
                try
                {
                    CurrenciesByDate = GetCurrenciesByDate(Start);
                } catch(DateTooPastException e)
                {
                    Log.Warn("Date is too past", e);
                    return Currencies;
                }
                foreach (Currency Currency in CurrenciesByDate)
                {
                    Currency.ExchangeRateDate = DateTime.SpecifyKind(Start.Date, DateTimeKind.Utc);
                }
                Currencies.AddRange(CurrenciesByDate);
                Start = Start.AddDays(1);
            }
            Log.InfoFormat("Currencies size {0} by start date {1} to end date {2}", Currencies.Count, Start.Date, End.Date);
            Log.Info("GetCurrenciesByStartAndEndDate() in Client - finish");
            return Currencies;
        }
    }
}