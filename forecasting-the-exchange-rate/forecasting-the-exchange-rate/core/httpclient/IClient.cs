﻿using forecasting_the_exchange_rate.Models;
using System;
using System.Collections.Generic;

namespace forecasting_the_exchange_rate.core.httpclient
{
    public interface IClient
    {
        List<Currency> GetCurrenciesByDate(DateTime DateTime);
        List<Currency> GetCurrenciesByStartAndEndDate(DateTime Start, DateTime End);
    }
}
