﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace forecasting_the_exchange_rate.core.httpclient
{
    public class DateTooPastException : Exception
    {
        public DateTooPastException() : base()
        {
        }
        public DateTooPastException(String Message) : base(Message)
        {
        }
        public DateTooPastException(String Message, Exception Exception) : base(Message, Exception)
        {
        }
    }
}