﻿using Quartz;
using Quartz.Spi;
using Unity;

namespace forecasting_the_exchange_rate.core.scheduler
{
    public class UnityJobFactory : IJobFactory
    {
        private readonly IUnityContainer UnityContainer;

        public UnityJobFactory(IUnityContainer UnityContainer)
        {
            this.UnityContainer = UnityContainer;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return UnityContainer.Resolve(bundle.JobDetail.JobType) as IJob;
        }

        public void ReturnJob(IJob job)
        {
        }
    }
}