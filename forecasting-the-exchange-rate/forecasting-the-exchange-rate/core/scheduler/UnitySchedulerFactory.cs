﻿using Quartz;
using Quartz.Core;
using Quartz.Impl;
using Quartz.Spi;

namespace forecasting_the_exchange_rate.core.scheduler
{
    public class UnitySchedulerFactory : StdSchedulerFactory
    {
        private readonly IJobFactory JobFactory;

        public UnitySchedulerFactory(IJobFactory JobFactory)
        {
            this.JobFactory = JobFactory;
        }

        protected override IScheduler Instantiate(QuartzSchedulerResources rsrcs, QuartzScheduler qs)
        {
            qs.JobFactory = JobFactory;
            return base.Instantiate(rsrcs, qs);
        }
    }
}