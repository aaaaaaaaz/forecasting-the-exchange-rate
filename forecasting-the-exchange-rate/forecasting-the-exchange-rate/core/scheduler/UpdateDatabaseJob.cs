﻿using forecasting_the_exchange_rate.core.httpclient;
using forecasting_the_exchange_rate.Models;
using forecasting_the_exchange_rate.Repository;
using log4net;
using Quartz;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace forecasting_the_exchange_rate.core.scheduler
{
    public class UpdateDatabaseJob : IJob
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ICurrencyRepository CurrencyRepository;
        private readonly IClient HttpClient;

        public UpdateDatabaseJob(ICurrencyRepository CurrencyRepository, IClient HttpClient)
        {
            this.CurrencyRepository = CurrencyRepository;
            this.HttpClient = HttpClient;
        }

        public Task Execute(IJobExecutionContext context)
        {
            Log.Info("Execute() in UpdateDatabaseJob - start");
            List<Currency> Currencies = HttpClient.GetCurrenciesByDate(DateTime.Today.Date);
            Log.InfoFormat("Number of the latest exchange rates {0}", Currencies.Count);
            CurrencyRepository.AddList(Currencies);
            return Task.Run(() => { Log.Info("Execute() in UpdateDatabaseJob - finish"); });
        }
    }
}