﻿using System;

namespace forecasting_the_exchange_rate.core.exchange_rate_prediction.regression
{
    public class Point
    {
        public decimal X { get; }
        public decimal Y { get; }

        public Point(decimal X, decimal Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Point P = (Point)obj;
                return (X == P.X) && (Y == P.Y);
            }
        }

        public override int GetHashCode()
        {
            int Hash = 13;
            Hash = (Hash * 7) + X.GetHashCode();
            Hash = (Hash * 7) + Y.GetHashCode();
            return Hash;
        }
    }
}