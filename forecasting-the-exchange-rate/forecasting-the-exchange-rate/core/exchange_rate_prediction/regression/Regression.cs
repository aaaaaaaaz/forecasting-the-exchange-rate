﻿using System;
using System.Collections.Generic;

namespace forecasting_the_exchange_rate.core.exchange_rate_prediction.regression
{
    public class Regression
    {
        private decimal Slope;
        private decimal Intercept;

        private readonly List<Point> SamplePoints;
        private decimal XMean;
        private decimal YMean;

        public Regression(List<Point> SamplePoints)
        {
            if (SamplePoints.Count < 3)
            {
                throw new NotEnoughPointsException("Number of points should be greater than or equal to 3");
            }

            this.SamplePoints = SamplePoints;
            SetMeans(SamplePoints);
        }

        private void SetMeans(List<Point> SamplePoints)
        {
            foreach (Point point in SamplePoints)
            {
                XMean += point.X;
                YMean += point.Y;
            }
            XMean /= SamplePoints.Count;
            YMean /= SamplePoints.Count;
        }

        private decimal Covariance()
        {
            decimal Product = 0.0m;
            foreach (Point point in SamplePoints)
            {
                Product += (point.X - XMean) * (point.Y - YMean);
            }
            return Product / (SamplePoints.Count - 1);
        }

        private decimal VarianceX()
        {
            decimal Product = 0.0m;
            foreach (Point point in SamplePoints)
            {
                Product += (point.X - XMean) * (point.X - XMean);
            }
            return Product / (SamplePoints.Count - 1);
        }

        public decimal getPrediction(decimal LastExchangeRate)
        {
            Slope = Covariance() / VarianceX();
            Intercept = YMean - Slope * XMean;
            return Intercept + LastExchangeRate * Slope;
        }
    }
}