﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace forecasting_the_exchange_rate.core.exchange_rate_prediction.regression
{
    public static class TimeSeriesLag
    {
        public static List<Point> Lag(List<decimal> Values)
        {
            List<decimal> Predictions = Values.GetRange(0, Values.Count - 1);
            List<decimal> Outputs = Values.GetRange(1, Values.Count - 1);

            List<Point> points = new List<Point>();
            for (int i = 0; i < Predictions.Count; i++)
            {
                points.Add(new Point(Predictions.ElementAt(i), Outputs.ElementAt(i)));
            }
            return points;
        }
    }
}