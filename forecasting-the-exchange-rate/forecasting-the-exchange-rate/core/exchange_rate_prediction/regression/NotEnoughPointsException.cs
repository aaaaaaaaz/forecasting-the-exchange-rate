﻿using System;

namespace forecasting_the_exchange_rate.core.exchange_rate_prediction.regression
{
    public class NotEnoughPointsException : Exception
    {
        public NotEnoughPointsException() : base()
        {
        }

        public NotEnoughPointsException(String Message) : base(Message)
        {
        }

        public NotEnoughPointsException(String Message, Exception Exception) : base(Message, Exception)
        {
        }
    }
}