﻿using forecasting_the_exchange_rate.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace forecasting_the_exchange_rate.Repository
{
    public class CurrencyRepository : ICurrencyRepository
    {
        private readonly Context db;

        public CurrencyRepository(Context db)
        {
            this.db = db;
        }
        public IQueryable<Currency> Currencies { get { return db.Currencies; } }

        public void Add(Currency Currency)
        {
            db.Currencies.Add(Currency);
            db.SaveChanges();
        }

        public void Delete(Currency Currency)
        {
            db.Currencies.Remove(Currency);
            db.SaveChanges();
        }

        public void Delete(long CurrencyId)
        {
            Currency Currency = (from c in db.Currencies where c.CurrencyId == CurrencyId && c.Code != "PLN" select c).SingleOrDefault();
            db.Currencies.Remove(Currency);
        }

        public List<Currency> FindByCode(string Code)
        {
            List<Currency> CurrencyList = (from c in db.Currencies where c.Code == Code && c.Code != "PLN" orderby c.ExchangeRateDate ascending select c).ToList();
            if (CurrencyList == null)
            {
                return new List<Currency>();
            }
            else
            {
                return CurrencyList;
            }
        }

        public Currency FindById(long CurrencyId)
        {
            Currency Currency = (from c in db.Currencies where c.CurrencyId == CurrencyId && c.Code != "PLN" select c).SingleOrDefault();
            return Currency;
        }

        public Currency FindByDateAndCode(string Code, DateTime Date)
        {
            Currency Currency = (from c in db.Currencies where c.Code == Code && c.ExchangeRateDate == Date && c.Code != "PLN" select c).SingleOrDefault();
            return Currency;
        }

        public void Update(Currency Currency)
        {
            db.Entry(Currency).State = EntityState.Modified;
        }

        public void AddList(List<Currency> Currencies)
        {
            foreach (Currency Currency in Currencies)
            {
                Add(Currency);
            }
        }

        public List<Currency> FindToDateAndCode(string Code, DateTime Date)
        {
            List<Currency> CurrencyList = (from c in db.Currencies where c.Code == Code && c.ExchangeRateDate >= Date && c.Code != "PLN" select c).ToList();
            if (CurrencyList == null)
            {
                return new List<Currency>();
            }
            else
            {
                return CurrencyList;
            }
        }

        public List<Currency> FindByDate(DateTime Date)
        {
            List<Currency> CurrencyList = (from c in db.Currencies where c.ExchangeRateDate == Date && c.Code != "PLN" select c).ToList();
            return CurrencyList;
        }

        public List<String> FindAllDistinctCurrencyCodes()
        {
            List<String> CurrencyCodes = (from c in db.Currencies where c.Code != "PLN" select c.Code).Distinct().ToList();
            return CurrencyCodes;
        }

        public void ExecuteCommand(string command)
        {
            db.Database.ExecuteSqlCommand(command);
        }
    }
}