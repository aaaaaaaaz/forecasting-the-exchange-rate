﻿using forecasting_the_exchange_rate.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace forecasting_the_exchange_rate.Repository
{
    public interface ICurrencyRepository
    {
        IQueryable<Currency> Currencies { get; }
        void Add(Currency Currency);
        void AddList(List<Currency> Currencies);
        Currency FindById(long CurrencyId);
        List<Currency> FindByDate(DateTime Date);
        List<Currency> FindByCode(string Code);
        Currency FindByDateAndCode(string Code, DateTime Date);
        void Update(Currency Currency);
        void Delete(Currency Currency);
        void Delete(long CurrencyId);
        List<Currency> FindToDateAndCode(string Code, DateTime Date);
        List<String> FindAllDistinctCurrencyCodes();
        void ExecuteCommand(String command);
    }
}
