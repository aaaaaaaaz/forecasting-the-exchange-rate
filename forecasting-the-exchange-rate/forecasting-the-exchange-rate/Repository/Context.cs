﻿using forecasting_the_exchange_rate.Models;
using System.Data.Entity;

namespace forecasting_the_exchange_rate.Repository
{
    public class Context : DbContext
    {
        public Context() : base("exchange_rate_DB")
        {
            Database.Initialize(true);
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Przy pierwszym uruchomieniu bez wygenerowanej bazy DropCreateDatabaseIfModelChanges<Context>() zamienić na DropCreateDatabaseAlways<Context>()
            Database.SetInitializer<Context>(new DropCreateDatabaseIfModelChanges<Context>());
            modelBuilder.Entity<Currency>().Property(x => x.Mid).HasPrecision(8, 4);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Currency> Currencies { get; set; }
    }
}