﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace forecasting_the_exchange_rate.Models
{
    public class User
    {
        [Key, ScaffoldColumn(false)]
        public int UserId { get; set; }
        [Column("login")]
        public string Login { get; set; }
        [Column("password")]
        public string Password { get; set; }

        public User()
        {

        }

        public User(string Login, string Password)
        {
            this.Login = Login;
            this.Password = Password;
        }
    }
}