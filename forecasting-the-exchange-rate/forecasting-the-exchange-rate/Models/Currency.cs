﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace forecasting_the_exchange_rate.Models
{
    [Table("currency")]
    public class Currency
    {
        [Key, ScaffoldColumn(false)]
        public int CurrencyId { get; set; }
        [Column("code")]
        public string Code { get; set; }
        [Column("mid")]
        public decimal Mid { get; set; }
        [Column("exchange_rate_date")]
        public DateTime ExchangeRateDate { get; set; }

        public Currency()
        {
        }

        public Currency(string Code, decimal Mid, DateTime ExchangeRateDate) 
        {
            this.Code = Code;
            this.Mid = Mid;
            this.ExchangeRateDate = ExchangeRateDate;
        }
    }
}