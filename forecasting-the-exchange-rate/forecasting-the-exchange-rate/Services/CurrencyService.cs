﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using forecasting_the_exchange_rate.core.exchange_rate_prediction.regression;
using forecasting_the_exchange_rate.core.httpclient;
using forecasting_the_exchange_rate.Models;
using forecasting_the_exchange_rate.Repository;
using log4net;

namespace forecasting_the_exchange_rate.Services
{
    public class CurrencyService : ICurrencyService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ICurrencyRepository CurrencyRepository;
        private readonly IClient Client;

        public CurrencyService(ICurrencyRepository CurrencyRepository, IClient Client)
        {
            this.CurrencyRepository = CurrencyRepository;
            this.Client = Client;
        }

        public Currency FindByDateAndCode(string Code, DateTime Date)
        {
            Log.InfoFormat("FindByDateAndCode() in CurrencyService - Code: {0}, Date: {1}", Code, Date.Date);
            return CurrencyRepository.FindByDateAndCode(Code, Date);
        }

        public List<Currency> FindByCode(string Code)
        {
            Log.InfoFormat("FindByCode() in CurrencyService - Code: {0}", Code);
            return CurrencyRepository.FindByCode(Code);
        }

        public Currency PredictCurrencyByDate(string Code, DateTime Date)
        {
            Log.InfoFormat("PredictCurrencyByDate() in CurrencyService - Code: {0}, Date: {1}", Code, Date.Date);
            if (DateTime.Today.Date >= Date && FindByDateAndCode(Code, Date) != null)
            {
                return FindByDateAndCode(Code, Date);
            }
            List<Currency> Currencies = PredictCurrencyToDate(Code, Date);
            return Currencies[Currencies.Count - 1];
        }

        public List<Currency> PredictCurrencyToDate(string Code, DateTime Date)
        {
            Log.InfoFormat("PredictCurrencyToDate() in CurrencyService - Code: {0}, Date: {1}", Code, Date.Date);
            List<Currency> Currencies = FindByCode(Code);
            List<decimal> ExchangeRate = new List<decimal>();
            foreach (Currency Currency in FindByCode(Code))
            {
                ExchangeRate.Add(Currency.Mid);
            }
            for (int i = 0; i <= (Date - DateTime.Today.Date).Days; i++)
            {
                Regression Regression;
                try
                {
                    Regression = new Regression(TimeSeriesLag.Lag(ExchangeRate));
                } catch(NotEnoughPointsException e)
                {
                    Log.Warn("Not enough data to prediction exchange rate", e);
                    return Currencies;
                }
                decimal Prediction = Regression.getPrediction(ExchangeRate[ExchangeRate.Count - 1]);
                Prediction = decimal.Round(Prediction, 4, MidpointRounding.AwayFromZero);
                ExchangeRate.Add(Prediction);
                Currencies.Add(new Currency(Code, Prediction, DateTime.Today.Date.AddDays(i)));
            }
            return Currencies.Where(s => s.ExchangeRateDate > DateTime.Today.Date).ToList();
        }

        public void Add(Currency Currency)
        {
            Log.InfoFormat("Add() in CurrencyService - Currency - Code: {0}, Mid: {1}, ExchangeRateDate: {2}", Currency.Code, Currency.Mid, Currency.ExchangeRateDate);
            CurrencyRepository.Add(Currency);
        }

        public void AddList(List<Currency> Currencies)
        {
            Log.InfoFormat("AddList() in CurrencyService - Currencies size: {0}", Currencies.Count);
            CurrencyRepository.AddList(Currencies);
        }

        public Currency FindById(long CurrencyId)
        {
            Log.InfoFormat("FindById() in CurrencyService - Id: {0}", CurrencyId);
            return CurrencyRepository.FindById(CurrencyId);
        }

        public void Update(Currency Currency)
        {
            Log.InfoFormat("Update() in CurrencyService - Currency - Id: {0}, Code: {1}, Mid: {2}, ExchangeRateDate: {3}", Currency.CurrencyId, Currency.Code, Currency.Mid, Currency.ExchangeRateDate);
            CurrencyRepository.Update(Currency);
        }

        public void Delete(Currency Currency)
        {
            Log.InfoFormat("Delete() in CurrencyService - Currency - Id: {0}, Code: {1}, Mid: {2}, ExchangeRateDate: {3}", Currency.CurrencyId, Currency.Code, Currency.Mid, Currency.ExchangeRateDate);
            CurrencyRepository.Delete(Currency);
        }

        public void Delete(long CurrencyId)
        {
            Log.InfoFormat("Delete() by id in CurrencyService - Id: {0}", CurrencyId);
            CurrencyRepository.Delete(CurrencyId);
        }

        public Currency FindExchangeRateByDateAndCode(string Code, DateTime Date)
        {
            Log.InfoFormat("FindExchangeRateByDateAndCode() in CurrencyService - Code: {0}, Date: {1}", Code, Date.Date);
            return CurrencyRepository.FindByDateAndCode(Code, Date);
        }

        public List<Currency> FindExchangeRateToDateAndCode(string Code, DateTime Date)
        {
            Log.InfoFormat("FindExchangeRateToDateAndCode() in CurrencyService - Code: {0}, Date: {1}", Code, Date.Date);
            return CurrencyRepository.FindToDateAndCode(Code, Date);
        }

        public List<Currency> FindTheNewest()
        {
            List<Currency> Currencies = null;
            DateTime Date = DateTime.Today.Date;
            while ((Currencies == null || Currencies.Count == 0) && Date.Year >= 2018)
            {
                Currencies = CurrencyRepository.FindByDate(Date);
                Date = Date.AddDays(-1);
            }
            return Currencies;
        }

        public List<String> FindAllDistinctCurrencyCodes()
        {
            return CurrencyRepository.FindAllDistinctCurrencyCodes();
        }

        public void InitData()
        {
            CurrencyRepository.ExecuteCommand("TRUNCATE TABLE currency");
            CurrencyRepository.AddList(Client.GetCurrenciesByStartAndEndDate(new DateTime(2018, 1, 1), DateTime.Today.Date));
        }
    }
}