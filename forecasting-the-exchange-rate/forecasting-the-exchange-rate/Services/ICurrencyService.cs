﻿using forecasting_the_exchange_rate.Models;
using System;
using System.Collections.Generic;

namespace forecasting_the_exchange_rate.Services
{
    public interface ICurrencyService
    {
        void Add(Currency Currency);
        void AddList(List<Currency> Currencies);
        Currency FindById(long CurrencyId);
        void Update(Currency Currency);
        void Delete(Currency Currency);
        void Delete(long CurrencyId);
        Currency FindByDateAndCode(string Code, DateTime Date);
        List<Currency> FindByCode(string Code);
        Currency PredictCurrencyByDate(string Code, DateTime Date);
        List<Currency> PredictCurrencyToDate(string Code, DateTime Date);
        Currency FindExchangeRateByDateAndCode(string Code, DateTime Date);
        List<Currency> FindExchangeRateToDateAndCode(string Code, DateTime Date);
        List<Currency> FindTheNewest();
        List<String> FindAllDistinctCurrencyCodes();
        void InitData();
    }
}