using forecasting_the_exchange_rate.Controllers;
using forecasting_the_exchange_rate.core.httpclient;
using forecasting_the_exchange_rate.core.scheduler;
using forecasting_the_exchange_rate.Repository;
using forecasting_the_exchange_rate.Services;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using Quartz.Spi;
using System.Web.Http;
using Unity;
using Unity.WebApi;


namespace forecasting_the_exchange_rate
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            container.RegisterType<ICurrencyRepository, CurrencyRepository>();
            container.RegisterType<ICurrencyService, CurrencyService>();
            container.RegisterType<IClient, Client>();
            container.RegisterType<CurrencyController>();
            container.RegisterType<IJobFactory, UnityJobFactory>();
            container.RegisterType<ISchedulerFactory, UnitySchedulerFactory>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
            StartScheduler(container);
        }

        public static void StartScheduler(IUnityContainer UnityContainer)
        {
            var Scheduler = UnityContainer.Resolve<ISchedulerFactory>().GetScheduler().Result;
            Scheduler.ScheduleJob(
                new JobDetailImpl("UpdateDatabaseJob", typeof(UpdateDatabaseJob)),
                new CronTriggerImpl("UpdateDatebaseTrigger", "UpdateDatabaseGroup", "0 0 19 * * ?")
            );
            Scheduler.Start();
        }
    }
}