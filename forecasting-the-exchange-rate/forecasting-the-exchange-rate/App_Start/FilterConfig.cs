﻿using System.Web;
using System.Web.Mvc;

namespace forecasting_the_exchange_rate
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
