﻿using System.Collections.Generic;

namespace forecasting_the_exchange_rate.poco
{
    public class RootObject
    {
        public string date { get; set; }
        public Rates rates { get; set; }
        public string @base { get; set; }
    }
}