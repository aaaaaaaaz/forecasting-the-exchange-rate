﻿using forecasting_the_exchange_rate.core.exchange_rate_prediction.regression;
using NUnit.Framework;
using System;

namespace forecasting_the_exchange_rate.Tests
{
    public class PointTest
    {
        [Test]
        public void CreatePointTest()
        {
            decimal ExpectedX = 1.23m;
            decimal ExpectedY = 2.34m;

            Point Point = new Point(1.23m, 2.34m);
            Assert.AreEqual(Point.X, ExpectedX);
            Assert.AreEqual(Point.Y, ExpectedY);
        }
    }
}