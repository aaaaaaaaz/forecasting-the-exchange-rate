﻿using forecasting_the_exchange_rate.Models;
using NUnit.Framework;
using System;

namespace forecasting_the_exchange_rate.Tests
{
    public class CurrencyTest
    {
        [Test]
        public void CreateCurrencyTest()
        {
            string ExpectedCode = "EUR";
            decimal ExpectedMid = 2.53m;
            DateTime ExpectedExchangeRateDate = new DateTime(2018, 11, 11);

            Currency Currency = new Currency("EUR", 2.53m, new DateTime(2018, 11, 11));
            Assert.AreEqual(Currency.Code, ExpectedCode);
            Assert.AreEqual(Currency.Mid, ExpectedMid);
            Assert.AreEqual(Currency.ExchangeRateDate, ExpectedExchangeRateDate);
        }
    }
}