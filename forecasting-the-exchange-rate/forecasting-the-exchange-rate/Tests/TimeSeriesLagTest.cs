﻿using forecasting_the_exchange_rate.core.exchange_rate_prediction.regression;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace forecasting_the_exchange_rate.Tests
{
    public class TimeSeriesLagTest
    {
        [Test]
        public void LagTest()
        {
            List<Point> ExpectedPointsList = new List<Point>() { new Point(1.1m, 2.2m), new Point(2.2m, 3.3m), new Point(3.3m, 4.4m), new Point(4.4m, 5.5m) };

            List<decimal> Doubles = new List<decimal> { 1.1m, 2.2m, 3.3m, 4.4m, 5.5m };
            List<Point> Points = TimeSeriesLag.Lag(Doubles);
            Assert.AreEqual(Points, ExpectedPointsList);
        }
    }
}