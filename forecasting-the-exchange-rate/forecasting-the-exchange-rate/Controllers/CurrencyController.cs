﻿using forecasting_the_exchange_rate.Models;
using forecasting_the_exchange_rate.Services;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace forecasting_the_exchange_rate.Controllers
{
    public class CurrencyController : ApiController
    {
        private readonly ICurrencyService CurrencyService;

        public CurrencyController(ICurrencyService CurrencyService)
        {
            this.CurrencyService = CurrencyService;
        }

        [HttpGet]
        [Route("api/GetExchangeRateByDateAndCode/{Code}/{Date}")]
        public Currency FindExchangeRateByDateAndCode(string Code, DateTime Date)
        {
            return CurrencyService.FindExchangeRateByDateAndCode(Code, Date);
        }

        [HttpGet]
        [Route("api/GetExchangeRateToDateAndCode/{Code}/{Date}")]
        public List<Currency> FindExchangeRateToDateAndCode(string Code, DateTime Date)
        {
            return CurrencyService.FindExchangeRateToDateAndCode(Code, Date);
        }


        [HttpGet]
        [Route("api/GetPredictionByDateAndCode/{Code}/{Date}")]
        public Currency FindPredictionCurrencyByDateAndCode(string Code, DateTime Date)
        {
            return CurrencyService.PredictCurrencyByDate(Code, Date);
        }

        [HttpGet]
        [Route("api/GetPredictionToDateAndCode/{Code}/{Date}/")]
        public List<Currency> FindPredictionCurrencyToDateAndCode(string Code, DateTime Date)
        {
            return CurrencyService.PredictCurrencyToDate(Code, Date);
        }

        [HttpGet]
        [Route("api/GetTheNewest")]
        public List<Currency> FindTheNewest()
        {
            return CurrencyService.FindTheNewest();
        }

        [HttpGet]
        [Route("api/GetAllDistinctCurrencyCodes")]
        public List<String> FindAllDistinctCurrencyCodes()
        {
            return CurrencyService.FindAllDistinctCurrencyCodes();
        }


        [HttpGet]
        [Route("data/InitData")]
        public String InitData()
        {
            CurrencyService.InitData();
            return "it works";
        }
    }
}