import { Currency } from '../model/currency';

export class CurrencyUtil {
    public static getMinValueFromCurrencyList(currencies: Currency[]): number {
        let minValue: number = currencies[0].Mid;
        currencies.forEach((item) => {
            if (item.Mid < minValue) {
                minValue = item.Mid;
            }
        });
        return minValue;
    }

    public static getMaxValueFromCurrencyList(currencies: Currency[]): number {
        let maxValue: number = currencies[0].Mid;
        currencies.forEach((item) => {
            if (item.Mid > maxValue) {
                maxValue = item.Mid;
            }
        });
        return maxValue;
    }
}
