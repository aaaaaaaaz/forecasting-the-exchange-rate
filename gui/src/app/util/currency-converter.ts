import { Currency } from '../model/currency';
import { Data } from '../model/data';
import * as moment from 'moment';
import { Category } from '../model/category';

export class CurrencyConverter {
    public static convertToData(currencies: Currency[]): Data[] {
        let datas: Data[] = [];
        currencies.forEach((item) => {
            datas.push(new Data(item.Mid.toString()));
        });
        return datas;
    }

    public static convertToCategory(currencies: Currency[]): Category[] {
        let categories: Category[] = [];
        currencies.forEach((item) => {
            categories.push(new Category(moment(item.ExchangeRateDate).format('DD-MM-YYYY')));
        });
        return categories;
    }
}
