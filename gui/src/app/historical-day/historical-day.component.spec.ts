import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalDayComponent } from './historical-day.component';

describe('HistoricalDayComponent', () => {
  let component: HistoricalDayComponent;
  let fixture: ComponentFixture<HistoricalDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricalDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
