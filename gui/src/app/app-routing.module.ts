import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { HistoricalDayComponent } from './historical-day/historical-day.component';
import { HistoricalPeriodComponent } from './historical-period/historical-period.component';
import { PredictionDayComponent } from './prediction-day/prediction-day.component';
import { PredictionPeriodComponent } from './prediction-period/prediction-period.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'historical-day', component: HistoricalDayComponent },
  { path: 'historical-period', component: HistoricalPeriodComponent },
  { path: 'prediction-day', component: PredictionDayComponent },
  { path: 'prediction-period', component: PredictionPeriodComponent },
  { path: 'sign-in', component: SigninComponent },
  { path: 'sign-up', component: SignupComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
