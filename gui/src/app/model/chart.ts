export class Chart {
    theme = 'fusion';
    yAxisMinValue: string;
    yAxisMaxValue: string;
    labelDisplay = 'rotate';
    slantLabel = '1';
    decimals = '4';
    numVisiblePlot = '9';

    constructor(yAxisMinValue: string, yAxisMaxValue: string) {
        this.yAxisMinValue = yAxisMinValue;
        this.yAxisMaxValue = yAxisMaxValue;
    }
}
