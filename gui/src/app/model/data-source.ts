import { Chart } from './chart';
import { DataSourceCategory } from './data-source-category';
import { Dataset } from './dataset';
import { Category } from './category';
import { Data } from './data';

export class DataSource {
    chart: Chart;
    categories: DataSourceCategory[];
    dataset: Dataset[];

    constructor(minValue: number, maxValue: number, categories: Category[], datas: Data[]) {
        this.chart = new Chart(minValue.toString(), maxValue.toString());
        this.categories = [];
        this.categories.push(new DataSourceCategory());
        this.categories[0].category = categories;
        this.dataset = [];
        this.dataset.push(new Dataset());
        this.dataset[0].data = datas;
    }
}
