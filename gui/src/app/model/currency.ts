export class Currency {
    CurrencyId: number;
    Code: string;
    Mid: number;
    ExchangeRateDate: Date;
}
