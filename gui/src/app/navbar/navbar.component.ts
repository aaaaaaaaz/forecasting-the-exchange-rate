import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { StyleService } from '../service/style/style.service';
import { LogService } from '../service/log/log.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  style: string;
  name: string;

  constructor(private translateService: TranslateService, private styleService: StyleService, private logService: LogService) {
  }

  ngOnInit() {
    this.styleService.cast.subscribe((style: string) => {
      this.style = style;
    });
    this.logService.cast1.subscribe((name: string) => {
      this.name = name;
    });
  }

  changeLanguage(language: string): void {
    this.translateService.use(language);
  }

  changeStyle(style: string): void {
    localStorage.setItem('style', style);
    this.styleService.changeStyle(style);
  }

  logout(): void {
    localStorage.removeItem('name');
    this.logService.logout();
  }

}
