import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictionDayComponent } from './prediction-day.component';

describe('PredictionDayComponent', () => {
  let component: PredictionDayComponent;
  let fixture: ComponentFixture<PredictionDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictionDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictionDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
