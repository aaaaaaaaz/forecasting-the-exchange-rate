import { Component, OnInit } from '@angular/core';
import { CurrencyService } from '../service/currency/currency.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Currency } from '../model/currency';
import * as moment from 'moment';

@Component({
  selector: 'app-prediction-day',
  templateUrl: './prediction-day.component.html',
  styleUrls: ['./prediction-day.component.css']
})
export class PredictionDayComponent implements OnInit {

  currencyCodes: String[] = [];
  currency: Currency;

  currencyForm = new FormGroup({
    code: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[A-Z]{3}')])),
    date: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]{4}-[0-9]{2}-[0-9]{2}')]))
  });


  minDate: String = moment(new Date()).add(1, 'days').format('YYYY-MM-DD');
  maxDate: String = moment(new Date()).add(31, 'days').format('YYYY-MM-DD');

  isLoading: boolean;
  isShowing: boolean;

  constructor(private currencyService: CurrencyService) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.currencyService.findAllDistinctCurrencyCodes().subscribe((currencyCodes: String[]) => {
      this.currencyCodes = currencyCodes;
      this.isLoading = false;
    });
  }

  onClickSubmit(): void {
    this.isLoading = true;
    this.currencyService
    .findPredictionCurrencyByDateAndCode(this.currencyForm.value.code, this.currencyForm.value.date)
    .subscribe((currency: Currency) => {
      this.currency = currency;
      this.isShowing = true;
      this.isLoading = false;
    });
  }

}
