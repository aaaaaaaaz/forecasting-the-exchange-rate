import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LogService } from '../service/log/log.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  userForm = new FormGroup({
    name: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[A-Za-z0-9]{3,}')])),
    email: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,}@[A-Za-z]{3,}.[A-Za-z]{2,3}')])),
    password: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[A-Za-z0-9]{3,}')]))
  });

  constructor(private logService: LogService, private router: Router) { }

  ngOnInit() {
  }

  onClickSubmit(): void {
    this.logService.register(this.userForm.value.name, this.userForm.value.email, this.userForm.value.password);
    this.router.navigate(['/']);
  }

}
