import { Component, OnInit } from '@angular/core';
import { CurrencyService } from '../service/currency/currency.service';
import { Currency } from '../model/currency';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  currencies: Currency[] = [];
  currenciesDate: Date;

  isLoading: boolean;


  constructor(private currencyService: CurrencyService) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.currencyService.findTheNewest().subscribe((currencies: Currency[]) => {
      this.currencies = currencies;
      this.currenciesDate = currencies[0].ExchangeRateDate;
      this.isLoading = false;
    });
  }

}
