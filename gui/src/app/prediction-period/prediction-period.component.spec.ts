import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictionPeriodComponent } from './prediction-period.component';

describe('PredictionPeriodComponent', () => {
  let component: PredictionPeriodComponent;
  let fixture: ComponentFixture<PredictionPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictionPeriodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictionPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
