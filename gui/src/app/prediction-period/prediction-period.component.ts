import { Component, OnInit, OnChanges } from '@angular/core';
import { CurrencyService } from '../service/currency/currency.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Currency } from '../model/currency';
import * as moment from 'moment';
import { DataSource } from '../model/data-source';
import { CurrencyUtil } from '../util/currency-util';
import { CurrencyConverter } from '../util/currency-converter';

@Component({
  selector: 'app-prediction-period',
  templateUrl: './prediction-period.component.html',
  styleUrls: ['./prediction-period.component.css']
})
export class PredictionPeriodComponent implements OnInit {

  currencyCodes: String[] = [];
  currencies: Currency[] = [];
  currenciesCode: String;
  startDate: Date;
  endDate: Date;

  currencyForm = new FormGroup({
    code: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[A-Z]{3}')])),
    date: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]{4}-[0-9]{2}-[0-9]{2}')]))
  });

  minDate: String = moment(new Date()).add(1, 'days').format('YYYY-MM-DD');
  maxDate: String = moment(new Date()).add(31, 'days').format('YYYY-MM-DD');

  isLoading: boolean;
  isShowing: boolean;

  dataSource: Object;

  constructor(private currencyService: CurrencyService) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.currencyService.findAllDistinctCurrencyCodes().subscribe((currencyCodes: String[]) => {
      this.currencyCodes = currencyCodes;
      this.isLoading = false;
    });
  }


  onClickSubmit(): void {
    console.log(this.currencyForm.value.code);
    console.log(this.currencyForm.value.date);
    this.isLoading = true;
    this.currencyService
    .findPredictionCurrencyToDateAndCode(this.currencyForm.value.code, this.currencyForm.value.date)
    .subscribe((currencies: Currency[]) => {
      this.currencies = currencies;
      this.startDate = currencies[0].ExchangeRateDate;
      this.endDate = currencies[currencies.length - 1].ExchangeRateDate;
      this.currenciesCode = currencies[0].Code;
      this.isShowing = true;
      this.isLoading = false;
      this.dataSource = new DataSource(
        CurrencyUtil.getMinValueFromCurrencyList(this.currencies),
        CurrencyUtil.getMaxValueFromCurrencyList(this.currencies),
        CurrencyConverter.convertToCategory(this.currencies),
        CurrencyConverter.convertToData(this.currencies)
      );
    });
  }

}
