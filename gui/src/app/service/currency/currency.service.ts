import { Injectable } from '@angular/core';
import { Currency } from 'src/app/model/currency';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CurrencyService {

  private apiPath = 'http://localhost:50254';
  private newestExchangeRatePath = '/api/GetTheNewest';
  private currencyCodesPath = '/api/GetAllDistinctCurrencyCodes';
  private historicalExchangeRateDayPath = '/api/GetExchangeRateByDateAndCode';
  private historicalExchangeRatePeriodPath = '/api/GetExchangeRateToDateAndCode';
  private predictionExchangeRateDayPath = '/api/GetPredictionByDateAndCode';
  private predictionExchangeRatePeriodPath = '/api/GetPredictionToDateAndCode';

  constructor(private httpClient: HttpClient) {
  }

  findTheNewest(): Observable<Currency[]> {
    return this.httpClient.get<Currency[]>(this.apiPath + this.newestExchangeRatePath);
  }

  findAllDistinctCurrencyCodes(): Observable<String[]> {
    return this.httpClient.get<String[]>(this.apiPath + this.currencyCodesPath);
  }

  findExchangeRateByDateAndCode(code: string, date: String): Observable<Currency> {
    return this.httpClient.get<Currency>(this.apiPath + this.historicalExchangeRateDayPath + '/' + code + '/' + date);
  }

  findExchangeRateToDateAndCode(code: string, date: String): Observable<Currency[]> {
    return this.httpClient.get<Currency[]>(this.apiPath + this.historicalExchangeRatePeriodPath + '/' + code + '/' + date);
  }

  findPredictionCurrencyByDateAndCode(code: string, date: String): Observable<Currency> {
    return this.httpClient.get<Currency>(this.apiPath + this.predictionExchangeRateDayPath + '/' + code + '/' + date);
  }

  findPredictionCurrencyToDateAndCode(code: string, date: String): Observable<Currency[]> {
    return this.httpClient.get<Currency[]>(this.apiPath + this.predictionExchangeRatePeriodPath + '/' + code + '/' + date);
  }
}
