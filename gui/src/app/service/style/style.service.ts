import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class StyleService {

  private style = new BehaviorSubject<string>(localStorage.getItem('style'));
  cast = this.style.asObservable();

  constructor() { }

  changeStyle(style: string) {
    this.style.next(style);
  }
}
