import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LogService } from '../service/log/log.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  userForm = new FormGroup({
    email: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,}@[A-Za-z]{3,}.[A-Za-z]{2,3}')])),
    password: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[A-Za-z0-9]{3,}')]))
  });

  constructor(private logService: LogService, private router: Router) { }

  ngOnInit() {
  }

  onClickSubmit(): void {
    this.logService.login(this.userForm.value.email, this.userForm.value.password);
    this.router.navigate(['/']);
  }

}
